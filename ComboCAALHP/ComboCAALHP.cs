﻿using System;
using System.Collections.Generic;
using CAALHP.Library;
using CAALHP.Library.Brokers;
using CAALHP.Library.Config;
using CAALHP.Library.Hosts;
using CAALHP.Library.Managers;
using CAALHP.Library.Observer;
using CAALHP.Library.State;
using CAALHP.Library.Visitor;

namespace ComboCAALHP
{
    public class ComboCAALHP : ICAALHP, IDisposable
    {
        public IUserState UserState { get; set; }
        public IList<IHostManager> HostManagers { get; private set; }
        public EventManager EventManager { get; private set; }
        private ILifeCycleManager _lifeCycleManager;
        private ICAALHPBroker _broker;
            
        public ComboCAALHP()
        {
            _broker = new CAALHPBroker(this);
            UserState = new NotLoggedInState(this);
            EventManager = new EventManager();
            HostManagers = new List<IHostManager>();
            HostManagers.Add(new IceHostManager.IceHostManager(EventManager, _broker));
            HostManagers.Add(new MafHostManager.MafHostManager(EventManager, _broker));
            EventManager.EventSubject.UserLoggedIn += EventSubject_UserLoggedIn;
            EventManager.EventSubject.UserLoggedOut += EventSubject_UserLoggedOut;            
        }

        void EventSubject_UserLoggedIn(object sender, UserLoggedInEventArgs e)
        {
            UserState.UserLoggedIn(e.UserEvent.User);
        }

        void EventSubject_UserLoggedOut(object sender, UserLoggedOutEventArgs e)
        {
            UserState.UserLoggedOut();
        }

        public void StartApp(string appName)
        {
            foreach (var hostManager in HostManagers)
            {
                hostManager.AppHost.ShowLocalApp(appName);
            }
        }

        public void CloseApp(string appName)
        {
            foreach (var hostManager in HostManagers)
            {
                hostManager.AppHost.CloseApp(appName);
            }
        }

        public List<PluginConfig> GetListOfInstalledApps()
        {
            var list = UserState.GetListOfInstalledApps() as List<PluginConfig>;
            return list;
        }

        public IList<PluginConfig> GetListOfRunningApps()
        {
            var apps = new List<PluginConfig>();
            foreach (var hostManager in HostManagers)
            {
                apps.AddRange(hostManager.AppHost.GetLocalRunningApps());
            }
            return apps;
        }

        public IList<PluginConfig> GetListOfInstalledDeviceDrivers()
        {
            var deviceDrivers = new List<PluginConfig>();
            foreach (var hostManager in HostManagers)
            {
                deviceDrivers.AddRange(hostManager.DeviceDriverHost.GetLocalInstalledDeviceDrivers());
            }
            return deviceDrivers;
        }

        public void ActivateDeviceDrivers()
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            EventManager.EventSubject.UserLoggedIn -= EventSubject_UserLoggedIn;
            EventManager.EventSubject.UserLoggedOut -= EventSubject_UserLoggedOut;
            foreach (var hostmanager in HostManagers)
            {
                if (hostmanager.AppHost != null) hostmanager.AppHost.ShutDown();
                if (hostmanager.DeviceDriverHost != null) hostmanager.DeviceDriverHost.ShutDown();
                if (hostmanager.ServiceHost != null) hostmanager.ServiceHost.ShutDown();
            }
        }

        public void Accept(IHostVisitor visitor)
        {
            visitor.Visit(this);
            foreach (var hostManager in HostManagers)
            {
                hostManager.Accept(visitor);
            }
        }
    }
}
