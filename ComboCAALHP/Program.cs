﻿using System;

namespace ComboCAALHP
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Starting shared event manager");
            //var eventManager = new EventManager();
            Console.WriteLine("Starting CAALHP");
            using (var caalhp = new ComboCAALHP())
            {
                Console.WriteLine("System running...");
                Console.WriteLine("Press 'q' to close CAALHP");
                while (!Console.ReadKey().KeyChar.Equals('q'))
                {

                }
            }
            //Console.WriteLine("Press <ENTER> to exit");
            //Console.ReadLine();
        }

    }
}
