﻿param($installPath, $toolsPath, $package, $project)

$file2 = $project.ProjectItems.Item("Pipeline").ProjectItems.Item("HostViewAdapters").ProjectItems.Item("Plugins.HostViewAdapters.dll")

$copyToOutput2 = $file2.Properties.Item("CopyToOutputDirectory")
$copyToOutput2.Value = 2

$project.Object.References | Where-Object { $_.Name -eq 'HostViewAdapters-RefOnly' } | ForEach-Object { Write-Host "removing reference: " $_.Name }
$project.Object.References | Where-Object { $_.Name -eq 'HostViewAdapters-RefOnly' } | ForEach-Object { $_.Remove() }

$project.Object.References | Where-Object { $_.Name -eq 'Plugins.HostViewAdapters' } | ForEach-Object { Write-Host "setting copy local = false on : " $_.Name }
$project.Object.References | Where-Object { $_.Name -eq 'Plugins.HostViewAdapters' } | ForEach-Object { $_.CopyLocal = $false; }